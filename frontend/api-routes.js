// Initialize express router
let router = require('express').Router();
let apiController = require('./controllers/apiControllers');
       
    

// Set default API response
router.get('/', function (req, res) 
{
    res.json(
    {
       status: 'API Its Working',
       message: 'Welcome to RESTHub crafted with love!',
    });
});

//EndPoint para leer toda la base
router.get('/listallusers',function(req,res)
{
    console.log("leer:");
    console.log(req.body);

    apiController.getContactos(req,res);
});
//EndPoint para leer con filtro
// router.post('/leerAgenda/?idBusqueda',function(req,res)
// {
//     console.log("leer con filtro");
//     apiController.getContactosById(req,res);
// });

//EndPoint para crear nuevo usuario en la BD
router.post('/createNewUser/User',function(req,res)
{
    console.log(req.body);

    // apiController.searchByMail(req,res);

    // if(res==false)
    apiController.createNewUser(req,res);
});


// Export API routes
module.exports = router;