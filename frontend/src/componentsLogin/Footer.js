import React from 'react';
import './styles/Badge.css';
import '../componentsLogin/styles/Footer.css';


class Footer extends React.Component {
    render() {
        return (
            <div class="footer">
                <h6 class="footer-top">¿Preguntas? Llamanos al 0800-345-1375</h6>
                <ul class="footer-links">
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/support/412" >
                            Preguntas frecuentes
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com">
                            Centro de ayuda
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://jobs.netflix.com/jobs" >
                            Empleo
                        </a>
                    </li>
                </ul>
                <ul class="footer-links">
                    <li>
                        <a className="footer-item" href="/watch" >
                            Formas de ver
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/termsofuse" >
                            Términos de uso
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/privacy" >
                            Privacidad
                        </a>
                    </li>
                </ul>
                <ul class="footer-links">
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/privacy#cookies">
                            Preferencias de cookies
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/es/node/2101" >
                            Información corporativa
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/contactus" >
                            Contáctanos
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Footer;