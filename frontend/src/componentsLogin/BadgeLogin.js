import React from 'react';
import ApiController from '../controller/apiController';

class BadgeLogin extends React.Component {

  state = {};

  handleCheck = (e) => {

    let data = {
        mail : this.state.mail,
        password : this.state.password,
    };

    console.log(data);
    ApiController.validarUsuario(data);
  };

  onChangeMail = (e)=>{
    this.setState({mail : e.target.value});
  }

  onChangePassword = (e)=>{
    this.setState({password : e.target.value});
  }

  render() {
    return (
      <div className= "Login__Forms">
            <h2>Ya tenés usuario?</h2>

        <form>
          <div className="form-group">
            <label>Usuario</label>
            <input
              className="form-control"
              type="text"
              name="mail"
              value = {this.state.mail}
              onChange = {this.onChangeMail.bind(this)}
            />
          </div>

          <div className="form-group">
            <label>Contraseña</label>
            <input
              className="form-control"
              type="text"
              value = {this.state.password}
              onChange = {this.onChangePassword.bind(this)}
            />
          </div>

          <button type="button" onClick={this.handleCheck} className="btn btn-primary"> LOG IN </button>
        </form>
      </div>
    );
  }
}

export default BadgeLogin;