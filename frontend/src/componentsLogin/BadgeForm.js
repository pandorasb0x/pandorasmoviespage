import React from 'react';
import ApiController from '../controller/apiController';

class BadgeForm extends React.Component {

  state = {};

  handleSave = (e) => {

    let data = {
        name : this.state.name,
        lastName : this.state.lastName,
        mail : this.state.mail,
        password : this.state.password,
    };

    console.log(data);
    ApiController.crearUsuario(data);
  };

  onChangeName (e){
    this.setState({name : e.target.value});
  }

  onChangeLastName = (e)=>{
    this.setState({lastName : e.target.value});
  }

  onChangeMail = (e)=>{
    this.setState({mail : e.target.value});
  }

  onChangePassword = (e)=>{
    this.setState({password : e.target.value});
  }
  
  render() {
    return (
      <div className="Login__Forms">
        <h2 className="font-weight-light">Registrate!</h2>

        <form >
          <div className="form-group">
            <label>Nombre</label>
            <input
              className="form-control"
              type="text"
              name="name"
              value = {this.state.name}
              onChange = {this.onChangeName.bind(this)}
            />
          </div>

          <div className="form-group">
            <label>Apellido</label>
            <input
              className="form-control"
              type="text"
              name="lastName"
              value = {this.state.lastName}
              onChange = {this.onChangeLastName.bind(this)}
            />
          </div>

          <div className="form-group">
            <label>Email</label>
            <input
              className="form-control"
              type="email"
              name="email"
              value = {this.state.mail}
              onChange = {this.onChangeMail.bind(this)}
            />
          </div>

          <div className="form-group">
            <label>Contraseña</label>
            <input
              className="form-control"
              type="text"
              value = {this.state.password}
              onChange = {this.onChangePassword.bind(this)}
            />
          </div>

          <button type="button" onClick={this.handleSave} className="btn btn-primary">
            CREÁ TU USUARIO
          </button>
        </form>
      </div>
    );
  }
}

export default BadgeForm;
