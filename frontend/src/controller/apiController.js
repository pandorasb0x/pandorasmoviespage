import { Component } from 'react';
import Swal from 'sweetalert2';

const URL_SEARCH="https://api.themoviedb.org/3/search/movie?language=en-US&page=1&include_adult=false&";
const API_KEY="8df71898c1e2a2a6370eedb649e88d1d";
const URL_DISCOVER="https://api.themoviedb.org/3/discover/movie?language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1";

class MoviesSearcher extends Component {

  state = {redirect:false};

    bringPopular(movies) {
        const API_POPULARITY = "https://api.themoviedb.org/3/discover/movie?api_key=8df71898c1e2a2a6370eedb649e88d1d&sort_by=popularity.desc&page=1";
        
        fetch(API_POPULARITY)
        .then((response) => {
            return response.json()
        })
        .then(searchResults => {
            movies(searchResults.results);       
        })
    }

    searchByTerm(movies,searchTerm) {
        
        const API_SEARCH_BYTERM=`${URL_SEARCH}&api_key=${API_KEY}&query=${searchTerm}`;

        fetch(API_SEARCH_BYTERM)
        .then((response) => {
            return response.json()
          })
          .then(searchResults => {
             movies(searchResults.results);       
           })
    }

    searchByGenre(movies,genre) {
        
        const API_SEARCH_BYGENRE= `${URL_DISCOVER}&api_key=${API_KEY}&with_genres=${genre}`;

        console.log(API_SEARCH_BYGENRE);

        fetch(API_SEARCH_BYGENRE)
        .then((response) => {
            return response.json()
          })
          .then(searchResults => {
             movies(searchResults.results);       
           })
      }
      crearUsuario(data){

        console.log("a crearUsuario llega:",data);
        const endpoint= "http://localhost:8081/apilogin/createNewUser";

        fetch (endpoint,{
            method:'POST',
            mode:"cors",
            headers:{'Content-Type': 'application/json'},
            body:JSON.stringify(data)
        }).then ((response) => {
            console.log("response status",response.status);

            if(response.status === 200)
             {
              Swal.fire(
                'Usuario registrado con ÉXITO',
                'ya sos parte de MOVIX!',
                'success'
              );
             }

            if(response.status === 401)
             {   Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'El usuario ya existe :(',
              footer: 'Volvé a probar con otro.'
            })}
        })
      }

      validarUsuario(data){

        console.log("a validarUsuario llega:",data);
        const endpoint= "http://localhost:8081/apilogin/validateCredentials";

        fetch (endpoint,{
            method:'POST',
            mode:"cors",
            headers:{'Content-Type': 'application/json'},
            body:JSON.stringify(data)
        }).then ((response) => {
            console.log("response status",response.status);

            if(response.status === 200)
             {
                //usuario y constraseña correta
                 window.location="http://localhost:3000/login/movies";
            }
            if(response.status === 401)
             {  
                Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'USUARIO o CONTRASEÑA incorrectas :(',
              footer: 'Volvé a intentar.'
            })}
        })
      }
}
export default new MoviesSearcher();