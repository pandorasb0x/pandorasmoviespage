import React, { Component } from 'react';
import MoviesCarousel from '../componentsMovies/Carousel';
import MovieCard from '../componentsMovies/MovieCard';
import SearchBar from '../componentsMovies/SearchBar';
import './styles/Pages.css';
import MoviesSearcher from '../controller/apiController';


class Movies extends Component {
  constructor(props) {
    super(props)
    this.state = {
      movies : [],
    }
  }

  componentDidMount() {
    MoviesSearcher.bringPopular(this.fillCards.bind(this));

  }

  fillCards(moviesResult){
    const aux =[];

    moviesResult.forEach((movie)=>{
      const mov= <MovieCard key={movie.id} movie={movie}/>;
      aux.push(mov);
    });
    
    this.setState({movies: aux})
  }
  
  getSelected = (selectedOption) => {
     console.log(selectedOption);

      if(selectedOption.name==="searchTerm")
      MoviesSearcher.searchByTerm(this.fillCards.bind(this),selectedOption.value);
        else
      MoviesSearcher.searchByGenre(this.fillCards.bind(this),selectedOption.name);

  }
  

  render() {

  return(

      <div className="Login__hero">
        <MoviesCarousel/>
        <SearchBar getInfo={this.getSelected}/>
          <div style={{ textAlign:"center", paddingTop:"32px"}}>
          <div style={{ display:"inline-block"}}>{this.state.movies}</div>
        </div>
    </div>
   );
  }
}
export default Movies;