import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
  },
}));

export default function PaperSheet(props) {
  const classes = useStyles();

  return (
    
      <Paper className={classes.root} style={{width: "300px", marginTop: "10px"
    }}>
      <div  className="row">
        <div className="column" style={{paddingLeft: "20px"}}>
        <Grid container justify="left" alignItems="left">
         <Avatar alt="Remy Sharp" src="/images/unicornio.jpg" className={classes.avatar} />
        </Grid>
        </div>

        <div className="column" style={{paddingLeft: "20px"}}>
        <Typography component="p">
            {props.text}
        </Typography>
        <Typography variant="caption" display="block" gutterBottom>
            Today at {new Date().getHours()}:{new Date().getMinutes()} PM
        </Typography>
        </div>
        </div>
      </Paper>
    
  );
}

