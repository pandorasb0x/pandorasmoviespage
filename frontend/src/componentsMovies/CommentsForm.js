import React from 'react';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import {FaArrowCircleRight} from 'react-icons/fa';
import Button from 'react-bootstrap/Button';
import Comment from './Comment';

class CommentsForm extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {value: '', comments:[], movieCodes:[]};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
      }
    
      handleChange(event) {
        this.setState({value: event.target.value});
      }
    
      handleClick(event) {
 
        const newComment=this.state.value;
        const newMovieCode= this.props.id;

        const comments = [...this.state.comments, newComment]; 
        const movieCodes = [...this.state.movieCodes, newMovieCode]; 
        
        this.setState({
        value: '',
        comments: comments,
        movieCodes: movieCodes
        });

        console.log(comments);
        console.log(movieCodes);
  
      }

   
    render() {
  
    return(
  
        <div>
    
            <h5 className="quotes">Ya la viste? Contanos si te gustó</h5>
            <div>
                <Form inline>
                <FormControl name="theComment" value={this.state.value} onChange={this.handleChange}  type="text" placeholder="Dejanos acá tu opinión..."  style={{width: "300px", margin:"0px"}}/>
                <Button variant="outline-info"  onClick={this.handleClick}  style={{width: "300px"}}>Send <FaArrowCircleRight/></Button>
                </Form>
            </div>
            <div style={{ display:"inline-block"}}>

            { 
              this.state.movieCodes.map((code, index) => {
                const comment = this.state.comments[index];
                if(code===this.props.id)
                return(<Comment text={comment} key={( 1 + (Math.random() * (1000)))}/>); 
                else return(<div style={{display:"none"}}></div>);
              })              
            }

            </div>

      </div>
     );
    }
  }
  export default CommentsForm;

