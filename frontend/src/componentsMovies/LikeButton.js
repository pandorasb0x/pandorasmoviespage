import React from 'react';
import './styles/myStyles.css';
import {FaThumbsUp} from 'react-icons/fa';

class LikeButton extends React.Component {
    constructor() {
      super();
      this.state = {
        liked: false
      };
      this.handleClick = this.handleClick.bind(this);
    } 
    
    handleClick() {
      this.setState({
        liked: !this.state.liked
      });
    }
    
    render() {

        const bClass = this.state.liked ? 'likeButton' : 'notLikeButton' ;
        const buttonClass = `${bClass}`;
        const buttonText = this.state.liked ? 'Te' : 'Me';

        return (
            <div>
                <button className={buttonClass} onClick={this.handleClick}>
                    {buttonText} gusta <FaThumbsUp/></button>
            </div>
        );
    }
  }
export default LikeButton;