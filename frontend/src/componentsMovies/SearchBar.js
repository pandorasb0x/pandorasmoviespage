import React from 'react';
import './styles/myStyles.css';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import '../global.css';
import {FaSearch, FaArrowCircleRight} from 'react-icons/fa';
import { Navbar, Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';


class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: '',
                      name:''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.myhandleClick = this.myhandleClick.bind(this);
      }

      handleChange(event) {
        this.setState({value: event.target.value,
                      name:event.target.name});
      }
    
      handleClick(event) {
        this.props.getInfo(this.state);
    }

    myhandleClick(event){
        event.preventDefault();
        const info= {name:event.target.name,
                    value: event.target.value};

        this.props.getInfo(info);
    };

    render() {

        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                 
                    <Form inline>
                        <FormControl  value={this.state.value} onChange={this.handleChange}  type="text" placeholder="Buscá acá tu peli favorita!" className="mr-sm-2" name="searchTerm"/>
                        <Button  variant="outline-info" name="byTerm" onClick={this.handleClick}><FaSearch/></Button>
                    </Form>
                    {/* const horror= 27;
                    const comedy= 35;
                    const scienceFiction= 878;
                    const adventure= 12;
                    const action= 28; */}

                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="#features" name="27" onClick={this.myhandleClick}>Horror</Nav.Link>
                            <Nav.Link href="#pricing" name="35" onClick={this.myhandleClick}>Comedy</Nav.Link>
                            <Nav.Link href="#pricing" name="878" onClick={this.myhandleClick}>Science Fiction</Nav.Link>
                            <Nav.Link href="#pricing" name="12" onClick={this.myhandleClick}>Adventure</Nav.Link>
                            <Nav.Link href="#pricing" name="28" onClick={this.myhandleClick}>Action</Nav.Link>

                        </Nav>
                        <Nav>
                            {/* <Nav.Link href="#deets">LOG OUT <FaArrowCircleRight/> </Nav.Link> */}
                            <Link to="/login" style={{color:"white", textDecoration:"none"}}>LOG OUT <FaArrowCircleRight/> </Link>            

                            
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
};

export default SearchBar;

