import React from 'react';
import './styles/Navbar.css';

class Navbar extends React.Component {
  render() {
    return (
      <div className="NavContainer">
       <div className="NavLogoContainer"><div className="NavLogo"></div></div>
      </div>
    );
  }
}

export default Navbar;
