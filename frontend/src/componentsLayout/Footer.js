import React from 'react';
// import './styles/Badge.css';
import './styles/Footer.css';


class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <h6 className="footer-top">¿Preguntas? Llamanos al 0800-345-1375</h6>
                <ul className="footer-links">
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/support/412" >
                            Preguntas frecuentes
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com">
                            Centro de ayuda
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://jobs.netflix.com/jobs" >
                            Empleo
                        </a>
                    </li>
                </ul>
                <ul className="footer-links">
                    <li>
                        <a className="footer-item" href="/watch" >
                            Formas de ver
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/termsofuse" >
                            Términos de uso
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/privacy" >
                            Privacidad
                        </a>
                    </li>
                </ul>
                <ul className="footer-links">
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/legal/privacy#cookies">
                            Preferencias de cookies
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/es/node/2101" >
                            Información corporativa
                        </a>
                    </li>
                    <li>
                        <a className="footer-item" href="https://help.netflix.com/contactus" >
                            Contáctanos
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Footer;