import React from 'react';
import Login from './pages/Login';
import Movies from './pages/Movies';
import Layout from './componentsLayout/Layout';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

function App() {
  return(
    <BrowserRouter>
    <Layout>
      <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/login/movies" component={Movies}/>
      </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
