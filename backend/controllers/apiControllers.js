var Users = require('../models/UserModel');

let validateCredentials = (req, res) =>
{      
    const mailBuscado = req.body.mail;
    const passwordBuscado = req.body.password;

    Users.find({mail: mailBuscado, password: passwordBuscado})
    .then
    (
        (usuario)=>
        {
            if(usuario.length!=0){
                console.log("encontro");
                res.status(200).send();}
            else{
                console.log("no encontro: usuario o contraseña equivocada");
                res.status(401).send();
            }
        },
        (err)=>{console.log(err);}
    )       
};

let createNewUser = (req,res) =>
{
    var newUser = Users({
        name: req.body.name,
        lastName: req.body.lastName,
        mail: req.body.mail,
        password: req.body.password,
    });

    let mailBuscado = {mail: req.body.mail};

    Users.find(mailBuscado)
    .then
    (   
        (usuarioBuscado)=>
        {
            //si el usuario no existe, lo creo
            if(usuarioBuscado.length==0){
                newUser.save();
                res.status(200).send();
            }
            else   
                res.status(401).send();
        },
        (err)=>{console.log(err);}
    )
}

module.exports = {createNewUser,validateCredentials};

